import React, {useState} from 'react';
import {StyleSheet, View , FlatList } from 'react-native';
import {Header} from './src/Header'
import {AddToDo} from "./src/AddToDo";
import {ITodo} from "./src/types";
import {ToDo} from "./src/ToDo";


function App(): JSX.Element {
    const [title] = useState('To Do App')
    const [todos,setTodo] = useState<ITodo[]>([])
    const addTodo = (todo:ITodo) => {
        setTodo((prev)=>{
            return [...prev , todo]
        })
    }

    const removeTodo = (id:string) =>{

        setTodo(prev=>prev.filter(todo => todo.id !== id))
    }

   return (
        <View style={styles.container}>
            <Header title={title}/>
            <AddToDo addTodo={addTodo}/>
            <FlatList
                keyExtractor={item=>item.id.toString()}
                data={todos}
                renderItem={({item}:{item:ITodo})=><ToDo handlerRemoveTodo={removeTodo} id={item.id} title={item.title}/>}
            />
        </View>
  );
}

const styles = StyleSheet.create({
 container:{},
 flatListContainer :{

 }
});

export default App;
