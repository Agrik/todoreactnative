import React, {useState} from 'react'
import {View, StyleSheet, TextInput, Text, Pressable , Alert} from "react-native";
import {ITodo} from "./types";

export const AddToDo = (props:any) => {
    const { addTodo, title = 'Add To Do' } = props;
    const [todoText, setText] = useState('');
    const handlerAddTodo = (todo:ITodo) => {
       if (todoText.trim()) {
             addTodo(todo)
             setText('')
       }
       else{
          Alert.alert('Text input required ! ')
       }
    }
    return (
        <View style={styles.todoContainer}>
         <TextInput style={styles.addInput}

             value={todoText}
             onChangeText={(text) => setText(text)}
                    keyboardType="numeric"
             placeholder="Add a new task..."/>
            <Pressable  style={({ pressed }) => [
                {
                    backgroundColor: pressed
                        ? '#550080'
                        : 'rgb(15,15,16)'
                },
                styles.addButton
            ]}  onPress={()=>handlerAddTodo({
                id:new Date().toString(),
                title:todoText
            })}>
                <Text style={styles.text}>{title}</Text>
            </Pressable>
        </View>

    )

}
const styles = StyleSheet.create({
    todoContainer : {
        flexDirection:'row',
        width:'100%'
    },
    addButton:{
        alignItems: 'center',
        width:'25%',
        justifyContent: 'center',
        margin:4,
        height:40,
        borderRadius: 4,
    },
    addInput:{
        width:'70%',
        borderStyle:'solid',
        borderWidth:1,
        margin:5,
        height:40,
        borderBottomColor:'black',
        borderRightColor:'black',
        borderRadius:4,
        padding: 10,
    }
    ,text:{
        color:'white',
        marginLeft:2
    }
})

