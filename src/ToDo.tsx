import React from 'react'
import {Button, Text, View, StyleSheet, TouchableOpacity} from "react-native";

export const ToDo = (props:any) => {
    const {id, title , handlerRemoveTodo} = props
    return (
        <View style={styles.container} key={id}>
             <View style={styles.todoTextContainer}><Text style={styles.text} numberOfLines={1}>{title}</Text></View>
            <TouchableOpacity onPress={()=>handlerRemoveTodo(id)} style={styles.button}>
                <Text style={styles.buttonText}>Delete</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        margin:4,
        justifyContent: 'space-between',
        alignItems: 'center',
        borderColor: '#ccc',
        borderWidth: 1,
        borderRadius: 5,
        padding: 10,
        marginBottom: 10,
    },
    text:{
        fontSize:15
    },
    todoTextContainer:{
        width:"80%"
    },
    buttonText: {
        color: '#FFF',
        fontWeight: 'bold',
    },
    button: {
        backgroundColor: '#FF4500',
        padding: 5,
        borderRadius: 5,
    }
})
