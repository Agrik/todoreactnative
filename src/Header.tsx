import {Text, View , StyleSheet} from "react-native";
import React from 'react';


export const Header = ({title} : {title:string}) => {
    return (
        <View style={styles.headerContainer}>
            <Text style={styles.headerText}>{title}</Text>
        </View>
);
}

const styles = StyleSheet.create({
    headerContainer: {
     height:85,
     backgroundColor:"#550080",
     alignItems:'center',
     justifyContent:'flex-end',
        paddingBottom:2,
        marginBottom:1
    },
    headerText: {
 color:'white',
        marginBottom:5
    },
})


